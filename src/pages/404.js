import Kosaki from "../pages/404.jpg"
import Layout from "../components/layout"
import React from "react"
import SEO from "../components/seo"
import { graphql } from "gatsby"

const NotFoundPage = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title

  return (
    <Layout location={location} title={siteTitle}>
      <SEO title="404: Not Found" />
      <h1 className="mt-16 mb-8 text-5xl font-black">Not Found</h1>
      <p className="mb-8">
        You just hit a route that doesn&#39;t exist... the sadness.
      </p>
      <img src={Kosaki} alt="404: Not Found" />
    </Layout>
  )
}

export default NotFoundPage

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`
