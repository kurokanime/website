import { Link } from "gatsby"
import React from "react"

const Layout = ({ location, title, children }) => {
  let header

  header = (
    <div>
      <div className="flex flex-wrap items-center ">
        <Link to={`/`}>
          <div className="text-sekai">
            <h1>Kurokanime</h1>
            Kami Hanyalah Sebuah Fanshare<br></br>
            Yang Bercita-cita Menjadi Fansub
          </div>
        </Link>
      </div>
      <hr></hr>
      <div className="text-2xl">
        <a href="https://rebrand.ly/kurokanime-facebook">Facebook</a> &nbsp;
        <a href="https://rebrand.ly/kurokanime-telegram">Telegram</a> &nbsp;
        <a href="https://rebrand.ly/kurokanime-discord">Discord</a> &nbsp;
        <a href="https://gitlab.com/kurokanime">GitLab</a>
      </div>
    </div>
  )

  return (
    // <div className="bg-sekai">
    <div className="max-w-3xl px-5 py-5 mx-auto bg-white border-2 rounded-lg shadow-xl">
      <header>{header}</header>
      <main>{children}</main>
      <footer className="mt-8">
        <div className="text-sm">
          Donasi
          <br />
          <a href="https://www.paypal.me/ffebriansyah">Paypal</a>
          <br />
          <a href="https://www.jenius.com/">Jenius By BTPN</a> : 9002 0477 988
          <br />
          <br />
        </div>
        <div className="text-lg">
          © 2020 - {new Date().getFullYear()} Kurokanime, Built with
          {` `}
          <a href="https://www.gatsbyjs.org">Gatsby</a>
          <br />
          <a href="https://gitlab.com/kurokanime/website">
            Source Code on GitLab
          </a>
          <br />
          <a href="https://gitlab.com/sekaidotid/website">
            Clone From SEKAI.ID Website
          </a>
        </div>
      </footer>
    </div>
    // </div>
  )
}

export default Layout
