---
title: "Anime Shachou, Battle no Jikan Desu! Subtitle Indonesia"
date: "2020-06-06T01:45:06+07:00"
---

## Judul

Shachou, Battle no Jikan Desu!

## Judul Bahasa Inggris

Shachibato! President, It's Time for Battle!

## Sinopsis

Diadaptasi dari game yang berlatar di dunia fantasi (isekai), pemain bertindak menjadi direktur yang dapat merekrut dan melatih para petualang (ksatria, penyihir, pemburu). Para petualang itu dikirim ke ruang bawah tanah untuk melawan monster dan mengamankan harta karun.

## Genre

Action, Adventure, Fantasy

## Sumber Fansub

ExTonan, Haruzora

# Unduh

## Episode 1

### nHD

MirrorAce

Mirroredto

### qHD

MirrorAce

Mirroredto

### HD

[MirrorAce](https://ouo.io/sWcu6q)

Mirroredto
