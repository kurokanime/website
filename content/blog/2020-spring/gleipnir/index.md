---
title: "Anime Gleipnir Subtitle Indonesia"
date: "2020-06-04T16:23:06+07:00"
---

## Judul

Gleipnir

## Judul Bahasa Inggris

Gleipnir

## Sinopsis

Shuichi Kagaya, anak SMA biasa di sebuah kota kecil yang membosankan. Namun, ketika teman kelasnya yang cantik terperangkap dalam kebakaran, Shuichi menyadari dirinya memiliki kekuatan misterius. Ia dapat berubah menjadi anjing berbulu lebat dengan pistol besar dan memiliki ritsleting di punggungnya. Ia pun menyelamatkan gadis itu, dan berbagi rahasia dengannya.

## Genre

Action, Ecchi, Mystery, Seinen, Supernatural

## Sumber Fansub

ExTonan, Anitoki

# Unduh

## Episode 1

### nHD

MirrorAce

Mirroredto

### qHD

MirrorAce

Mirroredto

### HD

MirrorAce

Mirroredto
